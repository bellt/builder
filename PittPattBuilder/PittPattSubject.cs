﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterfaceBuilder;

namespace PittPattBuilder
{
   public class PittPattSubject : ISubject
   {
      public PittPattSubject()
      {
         _id = _rand.Next();
      }

      public int id
      {
         get { return _id; }
         set { _id = value; } 
      }

      private int _id = 0;
      private static Random _rand = new Random();
   }
}
