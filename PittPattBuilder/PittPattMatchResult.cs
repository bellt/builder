﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterfaceBuilder;

namespace PittPattBuilder
{
   public class PittPattMatchResult : IMatchResult
   {
      public double score
      {
         get { return _score; }
         set { _score = value; }
      }

      public ISubject subject
      {
         get { return _subject; }
         set { _subject = value; }
      }

      private ISubject _subject = null;
      private double _score = 0;
   }
}
