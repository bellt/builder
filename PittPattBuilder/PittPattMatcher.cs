﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using InterfaceBuilder;

namespace PittPattBuilder
{
   public class PittPattMatcher : IBuilder
   {

      public PittPattMatcher(String models)
      {
         // set the models path.
         _modelspath = models;
      }

      /// <summary>
      /// Creates the builder
      /// </summary>
      /// <returns></returns>
      public status initialize_builder()
      {
         status s = status.FAILURE;
         if (!_sdk && _context == null)
         {
            ppr_settings_type settings = pittpatt_wrapper.ppr_get_default_settings();

            // Detection
            settings.detection.enable = 1;
            settings.detection.use_serial_face_detection = 0; // batch mode.
            settings.detection.num_threads = 4; // threads to use.

            // Recognition
            settings.recognition.enable_extraction = 1; // enables template extraction
            settings.recognition.enable_comparison = 1; // enables to build the gallery
            settings.recognition.recognizer = ppr_recognizer_type.PPR_RECOGNIZER_MULTI_POSE; // +-36 degrees of yaw.
            settings.recognition.num_comparison_threads = 4; // threads for recognition
            settings.recognition.automatically_extract_templates = 1; // template extraction during detection.

            // Landmarks
            settings.landmarks.enable = 1; // enables landmark detection.
            settings.landmarks.manually_detect_landmarks = 0; // automatically detected during detection
            settings.landmarks.landmark_range = ppr_landmark_range_type.PPR_LANDMARK_RANGE_COMPREHENSIVE;
          
            SWIGTYPE_p_p_ppr_context_struct pointer_context = pittpatt_wrapper.new_PprContextHandle();

            // initialize the sdk and context
            if (checkSuccess(pittpatt_wrapper.ppr_initialize_sdk(_modelspath, _licenseid, _licensekey)))
            {
               if (checkSuccess(pittpatt_wrapper.ppr_initialize_context(settings, pointer_context)))
               {
                  _context = pittpatt_wrapper.PprContextHandle_value(pointer_context);
                  _sdk = true;
                  s = status.SUCCESS;
               }
            }
         }
         return s;
      }

      public bool is_Initialized()
      {
         bool val = false;
         if (_sdk && _context != null)
         {
            val = true;
         }
         return val;
      }

      /// <summary>
      /// Releases the sdk and context objects.
      /// </summary>
      /// <returns></returns>
      public status destroy_builder()
      {
         status s = status.FAILURE;
         if (_context != null && _sdk)
         {
            pittpatt_wrapper.ppr_finalize_context(_context);
            pittpatt_wrapper.ppr_finalize_sdk();

            _context = null; // is this okay?
            _sdk = false;
            _subjectdict.Clear();

            s = status.SUCCESS;
         }
         return s;
      }

      public SWIGTYPE_p_ppr_context_struct getContext()
      {
         if (_context != null)
         {
            return _context;
         }
         else
         {
            throw new Exception("Builder Error: Context is null");
         }
      }

      /// <summary>
      /// Writes the binary face data to the given path.
      /// </summary>
      /// <param name="face"></param>
      /// <param name="path"></param>
      /// <returns></returns>
      public String writeFaceBuffer(SWIGTYPE_p_ppr_face_struct face, String path)
      {
         String val = null;
         try
         {
            ppr_flat_data_type flatdata = new ppr_flat_data_type();
            if (checkSuccess(pittpatt_wrapper.ppr_flatten_face(_context, face, flatdata)))
            {
               if (checkSuccess(pittpatt_wrapper.ppr_write_flat_data(_context, path, flatdata)))
               {
                  // free structure
                  pittpatt_wrapper.ppr_free_flat_data(flatdata);
                  val = path;
               }
            }
         }
         catch (Exception ex)
         {
         } 
         return val;
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="imagefiles"></param>
      /// <returns></returns>
      private status addToDetectionQueue(IEnumerable<String> imagefiles)
      {
         status val = status.SUCCESS;
         foreach (String imagepath in imagefiles)
         {
            var raw_imagetype = new ppr_raw_image_type();
            if (checkSuccess(pittpatt_wrapper.ppr_raw_image_io_read(imagepath, raw_imagetype)))
            {
               SWIGTYPE_p_p_ppr_image_struct pp_image = pittpatt_wrapper.new_PprImageHandle();

               /* Convert raw image to pittpatt image type */
               pittpatt_wrapper.ppr_create_image(raw_imagetype, pp_image);
               SWIGTYPE_p_ppr_image_struct image = pittpatt_wrapper.PprImageHandle_value(pp_image);

               if (checkSuccess(pittpatt_wrapper.ppr_add_image_to_detection_queue(_context, image)))
               {
                  // free image and raw images
                  pittpatt_wrapper.ppr_free_image(image);
                  pittpatt_wrapper.ppr_raw_image_free(raw_imagetype);
               }
            }
            else
            {
               val = status.FAILURE;
               break;
            }
         }
         return val;
      }

      /// <summary>
      /// Returns the detection results for each found face in an image.
      /// </summary>
      /// <param name="imagefiles"></param>
      /// <returns> </returns>
      public Dictionary<String, List<SWIGTYPE_p_ppr_face_struct>> detectFaces(List<String> imagefiles)
      {
         Dictionary<String, List<SWIGTYPE_p_ppr_face_struct>> detected = new Dictionary<string, List<SWIGTYPE_p_ppr_face_struct>>();

         // status is returned here.
         addToDetectionQueue(imagefiles);

         var ppr_image_results_list_type = new ppr_image_results_list_type();
         pittpatt_wrapper.ppr_get_detection_results(_context, ppr_image_results_list_type);

         ppr_face_list_type img_array = ppr_image_results_list_type.face_lists; // an array of face lists for each image.

         for (int i = 0; i < ppr_image_results_list_type.length; i++)
         {
            String imagefile = imagefiles[i];
            ppr_face_list_type faces = pittpatt_wrapper.image_array_getitem(img_array, i); // faces found in this image.
 
            List<SWIGTYPE_p_ppr_face_struct> face_list = new List<SWIGTYPE_p_ppr_face_struct>();
         
            // iterate through each face found in the image.
            for (int k = 0; k < faces.length; k++)
            {
               // pass in the same array with changing index to get the face object.
               SWIGTYPE_p_ppr_face_struct face = pittpatt_wrapper.face_array_getitem(faces.faces, k);
               face_list.Add(face);        
            }
            detected.Add(imagefile, face_list);
         }
         return detected;
      }

      /// <summary>
      /// Uses images to create a gallery object.
      /// </summary>
      /// <param name="imagefiles"></param>
      /// <returns></returns>
      public SWIGTYPE_p_ppr_gallery_struct createGalleryFromImage(IEnumerable<String> imagefiles)
      {
         SWIGTYPE_p_p_ppr_gallery_struct gallery = pittpatt_wrapper.new_PprGalleryHandle();
         SWIGTYPE_p_ppr_gallery_struct gallery_value = null;
         List<String> temp = new List<String>();
           
         // if gallery creation was successful.
         if(checkSuccess(pittpatt_wrapper.ppr_create_gallery(_context, gallery)))
         {
            addToDetectionQueue(imagefiles);

            foreach(String filename in imagefiles)
            {
               temp.Add(filename);
            }
            String[] array = temp.ToArray(); // assume this guarantees order.

            var image_results_list = new ppr_image_results_list_type();
            pittpatt_wrapper.ppr_get_detection_results(_context, image_results_list);
            ppr_face_list_type img_array = image_results_list.face_lists; // an array of face lists for each image.

            for (int i = 0; i < image_results_list.length; i++)
            {
               ppr_face_list_type faces = pittpatt_wrapper.image_array_getitem(img_array, i); // faces found in this image.
               List<ISubject> subjects = new List<ISubject>();
               String filename = array[i];

               // no filename repeats in the subject dictionary.
               if (!_subjectdict.ContainsKey(filename))
               {
                  _subjectdict.Add(filename, subjects);
               }

               // iterate through each face found in the image.
               for (int k = 0; k < faces.length; k++)
               {
                  // pass in the same array with changing index to get the face object.
                  SWIGTYPE_p_ppr_face_struct face = pittpatt_wrapper.face_array_getitem(faces.faces, k);

                  if (hasTemplate(face))
                  {
                     // create a PPSubject for every created template. Every found face.
                     PittPattSubject subject = new PittPattSubject();
                         
                     // add to the gallery.
                     pittpatt_wrapper.ppr_add_face(_context, gallery, face, subject.id, _rand.Next());

                     _subjectdict[filename].Add(subject); // add it to our list of subjects for this filename.
                     /* add the sdk_cmd_line_options_type to find unique faces in the image.
                        sdk_print_utils_print_gallery */
                  }
               } // face loop
            } // image loop
            gallery_value = pittpatt_wrapper.PprGalleryHandle_value(gallery);
            pittpatt_wrapper.ppr_free_image_results_list(image_results_list); // frees this structure, including the face lists, and the faces.           
         }
         return gallery_value;
      }

      /// <summary>
      /// Uses templates to create a gallery.
      /// </summary>
      /// <param name="templateFiles"></param>
      /// <returns></returns>
      public SWIGTYPE_p_ppr_gallery_struct createGalleryFromTemplate(IEnumerable<String> templateFiles)
      {
         SWIGTYPE_p_p_ppr_gallery_struct gallery = pittpatt_wrapper.new_PprGalleryHandle();
         SWIGTYPE_p_ppr_gallery_struct gallery_value = null;
         List<String> temp = new List<String>();

         // if gallery creation was successful.
         if (checkSuccess(pittpatt_wrapper.ppr_create_gallery(_context, gallery)))
         {
            foreach (String path in templateFiles)
            {
               ppr_flat_data_type flatdata = new ppr_flat_data_type();
               SWIGTYPE_p_p_ppr_face_struct face = pittpatt_wrapper.new_PprFaceHandle();
  
               pittpatt_wrapper.ppr_read_flat_data(_context, path, flatdata);
               pittpatt_wrapper.ppr_unflatten_face(_context, flatdata, face);
               SWIGTYPE_p_ppr_face_struct face_value = pittpatt_wrapper.PprFaceHandle_value(face);

               List<ISubject> subjects = new List<ISubject>();
               // no filename repeats in the subject dictionary.
               if (!_subjectdict.ContainsKey(path))
               {
                  _subjectdict.Add(path, subjects); 
               }

               PittPattSubject subject = new PittPattSubject();

               pittpatt_wrapper.ppr_add_face(_context, gallery, face_value, subject.id, _rand.Next()); // add to the gallery.
               _subjectdict[path].Add(subject); // add it to our list of subjects for this filename.

               // free structures.
               pittpatt_wrapper.ppr_free_flat_data(flatdata);
            }
            gallery_value = pittpatt_wrapper.PprGalleryHandle_value(gallery);
         }
         return gallery_value;
      }

      /// <summary>
      /// Checks the face to see if a template was generated.
      /// </summary>
      /// <param name="face"></param>
      /// <returns></returns>
      public bool hasTemplate(SWIGTYPE_p_ppr_face_struct face)
      {
         bool val = false;
         SWIGTYPE_p_int has_template = pittpatt_wrapper.new_IntHandle(); // int pointer
         pittpatt_wrapper.ppr_face_has_template(_context, face, has_template);
         int template_created = pittpatt_wrapper.IntHandle_value(has_template);

         // 0 if not.
         if (template_created == 1)
         {
            val = true;
         }
         return val;
      }

      /// <summary>
      /// Compares the galleries and returns a similarity matrix.
      /// </summary>
      /// <param name="query"></param>
      /// <param name="target"></param>
      /// <returns></returns>
      public SWIGTYPE_p_ppr_similarity_matrix_struct compareGalleries(SWIGTYPE_p_ppr_gallery_struct query, SWIGTYPE_p_ppr_gallery_struct target)
      {
         SWIGTYPE_p_p_ppr_similarity_matrix_struct matrix = pittpatt_wrapper.new_PprMatrixHandle();
         SWIGTYPE_p_ppr_similarity_matrix_struct matrix_value = null;
         if(checkSuccess(pittpatt_wrapper.ppr_compare_galleries(_context, query, target, matrix)))
         {
            matrix_value = pittpatt_wrapper.PprMatrixHandle_value(matrix);
         }
         return matrix_value;
      }

      public void freeGallery(SWIGTYPE_p_ppr_gallery_struct gallery)
      {
         try
         {
            pittpatt_wrapper.ppr_free_gallery(gallery); 
         }
         catch (Exception ex)
         {
         }  
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="gallery"></param>
      /// <param name="matrix"></param>
      /// <param name="subject"></param>
      /// <returns></returns>
      public IEnumerable<IMatchResult> match(SWIGTYPE_p_ppr_gallery_struct gallery, SWIGTYPE_p_ppr_similarity_matrix_struct matrix, ISubject subject)
      {
         ppr_score_list_type score_list = new ppr_score_list_type();
         List<IMatchResult> matches = new List<IMatchResult>();

         // takes the subject id.
         pittpatt_wrapper.ppr_get_ranked_subject_list(_context, matrix, subject.id, 10, -1.5F, score_list); // make threhsold and limit configurable.
         for (int i = 0; i < score_list.length; i++)
         {
            int sub_id = pittpatt_wrapper.int_array_getitem(score_list.ids,i); // this is the found subject id.
            float score = pittpatt_wrapper.float_array_getitem(score_list.scores,i);
            PittPattMatchResult match = new PittPattMatchResult();
            match.score = score;
            match.subject = findSubject(sub_id);
            matches.Add(match);
         }
         return matches;
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="subject_id"></param>
      /// <returns></returns>
      private ISubject findSubject(int subject_id)
      {
         ISubject val = null;
         bool breakout = false;
         foreach (var item in _subjectdict)
         {
            if (breakout)
            {
               break;
            }
            List<ISubject> subjects = item.Value;
            foreach(PittPattSubject entity in subjects)
            {
               if (entity.id == subject_id)
               {
                  val = entity;
                  breakout = true;
                  break;
               }
            }
         }
         return val;
      }

      private bool checkSuccess(ppr_raw_image_error_type error)
      {
         bool val = false;
         switch (error)
         {
            case ppr_raw_image_error_type.PPR_RAW_IMAGE_SUCCESS:
               val = true;
               break;
         }
         return val;
      }

      private bool checkSuccess(ppr_error_type error)
      {
         bool val = false;
         switch (error)
         {
            case ppr_error_type.PPR_SUCCESS:
               val = true;
               break;   
         }
         return val;
      }

      public Dictionary<String, List<ISubject>> getSubjectDictionary()
      {
         return _subjectdict;
      }

      private Random _rand = new Random();
      private SWIGTYPE_p_ppr_context_struct _context = null;
      private bool _sdk = false;
      private String _modelspath = "";
      private const String _licenseid = "scitor";
      private Dictionary<String, List<ISubject>> _subjectdict = new Dictionary<string,List<ISubject>>();
      private uint[] _licensekey = new uint[] { 0x023382bb, 0xabac2a2a, 0xffe8b57b, 0xb79e50e3, 0xc5c3a678, 0xa48893fd, 0xe7f05d1b, 0x9ee332e1, 0x8f24627b, 0x01e28566, 0xbe545fce, 0x2dec810d, 0xcc4982e8, 0x4db9a2ba, 0xcbe88aa7, 0xe819578f, 0x257265be, 0xb3962f54, 0xffdff4ca, 0xb31f9bb8, 0xff44864a, 0x79fe6ef8, 0x0bbd3ce7, 0xebad9468, 0x99ca4a79, 0x89373fe2, 0x4b6975be, 0x623dd237, 0x68465aca, 0x31a210d4, 0x46ec2b7a, 0x945638cd, 0x95349c13 };   
   }
}
