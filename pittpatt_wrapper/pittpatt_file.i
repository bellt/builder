/* This is the name of the generated dll. */
%module pittpatt_wrapper

/* Place directly into generated code. */
%{
#include "include/pittpatt_sdk.h"
#include "include/pittpatt_raw_image_io.h"
#include "include/pittpatt_video_io.h"
#include "include/pittpatt_license.h"
%}

/* Don't know what this does */
%include <windows.i>
%include "arrays_csharp.i"
%include "typemaps.i"
%include <cpointer.i>
%include <carrays.i>

%apply unsigned int INPUT[]  { const unsigned int * };

/* need this for the pointer to pointer conversions. */
%pointer_functions(ppr_context_struct *, PprContextHandle)
%pointer_functions(ppr_image_struct *, PprImageHandle)
%pointer_functions(ppr_gallery_struct *, PprGalleryHandle)
%pointer_functions(ppr_similarity_matrix_struct *, PprMatrixHandle)
%pointer_functions(ppr_face_struct *, PprFaceHandle);
%pointer_functions(int, IntHandle);
%pointer_functions(float, FloatHandle);

%array_functions(ppr_face_list_type , image_array);
%array_functions(ppr_face_struct *, face_array);
%array_functions(int, int_array);
%array_functions(float,float_array);

/* grab declarations from headers and put in generated */
%include "include/pittpatt_sdk.h"
%include "include/pittpatt_common.h"
%include "include/pittpatt_detection.h"
%include "include/pittpatt_errors.h"
%include "include/pittpatt_gallery_group.h"
%include "include/pittpatt_raw_image.h"
%include "include/pittpatt_recognition.h"
%include "include/pittpatt_types.h"
%include "include/pittpatt_raw_image_io.h"
%include "include/pittpatt_video_io.h"