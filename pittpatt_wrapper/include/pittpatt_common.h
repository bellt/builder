/**********************************************************************
 
  PittPatt Face Recognition Software Development Kit (PittPatt SDK)
  (C) Copyright 2004-2011 Pittsburgh Pattern Recognition

  This software is covered in part by the following patents:

     US Patent 6,829,384
     US Patent 7,194,114
     US Patent 7,848,566
     US Patent 7,881,505
     Pending US Patent Applications

  Portions of this product are manufactured under license from Carnegie
  Mellon University.

**********************************************************************/


#ifndef __PITTPATT_COMMON_H
#define __PITTPATT_COMMON_H

#include <stdio.h>

#include "pittpatt_types.h"
#include "pittpatt_errors.h"
#include "pittpatt_raw_image.h"

/* If C++ then we need to extern "C". Compiler defines __cplusplus */
#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 * Initialize Functions
 ******************************************************************************/

ppr_error_type ppr_initialize_sdk (const char *models_path,
                                   const char *license_id,
                                   const unsigned int *license_key);

ppr_settings_type ppr_get_default_settings (void);

ppr_error_type ppr_initialize_context (ppr_settings_type settings,
                                       ppr_context_type *context);

/*******************************************************************************
 * Face Functions
 ******************************************************************************/

ppr_error_type ppr_get_face_attributes (ppr_face_type face,
                                        ppr_face_attributes_type *face_attributes);

ppr_error_type ppr_get_face_landmarks (ppr_face_type face,
                                       ppr_landmark_list_type *landmark_list);

ppr_error_type ppr_remove_face_landmarks (ppr_context_type context,
                                          ppr_face_type *face);

ppr_error_type ppr_get_face_boundary (ppr_face_type face,
                                      ppr_bounding_box_style_type bounding_box_style,
                                      ppr_polygon_type *boundary);

ppr_error_type ppr_extract_face_thumbnail (ppr_context_type context,
                                           ppr_image_type image,
                                           ppr_face_type *face);

ppr_error_type ppr_get_face_thumbnail (ppr_context_type context,
                                       ppr_face_type face,
                                       ppr_raw_image_type *thumbnail);

ppr_error_type ppr_remove_face_thumbnail (ppr_context_type context,
                                          ppr_face_type *face);

ppr_error_type ppr_set_face_string (ppr_context_type context,
                                    ppr_face_type *face,
                                    const char *str);

ppr_error_type ppr_get_face_string (ppr_context_type context,
                                    ppr_face_type face,
                                    ppr_string_type *str);

ppr_error_type ppr_crop_face_from_image (ppr_context_type context,
                                         ppr_image_type image,
                                         ppr_face_type face,
                                         int width,
                                         int height,
                                         float padding,
                                         int rotate,
                                         ppr_raw_image_type *cropped_image);

/*******************************************************************************
 * Miscellaneous Functions
 ******************************************************************************/

ppr_error_type ppr_create_image (ppr_raw_image_type raw_image,
                                 ppr_image_type *image);

ppr_error_type ppr_image_to_raw_image (ppr_image_type image,
                                       ppr_raw_image_type *raw_image);

ppr_error_type ppr_get_size_from_pixels (int num_pixels,
                                         float *size);

ppr_error_type ppr_get_settings (ppr_context_type,
                                 ppr_settings_type *settings);

ppr_error_type ppr_create_id_list (int length,
                                   ppr_id_list_type *id_list);

ppr_error_type ppr_create_flat_data (size_t length,
                                     ppr_flat_data_type *flat_data);

ppr_error_type ppr_add_source_to_face_list (ppr_face_list_type *face_list,
                                            const char *source_name);

ppr_error_type ppr_add_source_to_track_list (ppr_track_list_type *track_list,
                                             const char *source_name);

const char* ppr_error_message (ppr_error_type error_code);

const char* ppr_version (void);

/*****************************************************************************
 * Flatten / Unflatten Functions
 *****************************************************************************/

ppr_error_type ppr_flatten_face (ppr_context_type context,
                                 ppr_face_type face,
                                 ppr_flat_data_type *flat_data);

ppr_error_type ppr_unflatten_face (ppr_context_type context,
                                   ppr_flat_data_type flat_data,
                                   ppr_face_type *face);

ppr_error_type ppr_flatten_face_list (ppr_context_type context,
                                      ppr_face_list_type face_list,
                                      ppr_flat_data_type *flat_data);

ppr_error_type ppr_unflatten_face_list (ppr_context_type context,
                                        ppr_flat_data_type flat_data,
                                        ppr_face_list_type *face_list);

ppr_error_type ppr_flatten_track_list (ppr_context_type context,
                                       ppr_track_list_type track_list,
                                       ppr_flat_data_type *flat_data);

ppr_error_type ppr_unflatten_track_list (ppr_context_type context,
                                         ppr_flat_data_type flat_data,
                                         ppr_track_list_type *track_list);

ppr_error_type ppr_flatten_gallery (ppr_context_type context,
                                    ppr_gallery_type gallery,
                                    ppr_flat_data_type *flat_data);

ppr_error_type ppr_unflatten_gallery (ppr_context_type context,
                                      ppr_flat_data_type flat_data,
                                      ppr_gallery_type *gallery);

ppr_error_type ppr_flatten_similarity_matrix (ppr_context_type context,
                                              ppr_similarity_matrix_type similarity_matrix,
                                              ppr_flat_data_type *flat_data);

ppr_error_type ppr_unflatten_similarity_matrix (ppr_context_type context,
                                                ppr_flat_data_type flat_data,
                                                ppr_gallery_type *query_gallery,
                                                ppr_gallery_type *target_gallery,
                                                ppr_similarity_matrix_type *similarity_matrix);

ppr_error_type ppr_flatten_gallery_group (ppr_context_type context,
                                          ppr_gallery_group_type gallery_group,
                                          ppr_flat_data_type *flat_data);

ppr_error_type ppr_unflatten_gallery_group (ppr_context_type context,
                                            ppr_flat_data_type flat_data,
                                            ppr_gallery_group_type *gallery_group);

/*******************************************************************************
 * File I/O Functions
 ******************************************************************************/

ppr_error_type ppr_write_flat_data (ppr_context_type context,
                                    const char *file_name,
                                    ppr_flat_data_type flat_data);

ppr_error_type ppr_read_flat_data (ppr_context_type context,
                                   const char *file_name,
                                   ppr_flat_data_type *flat_data);

ppr_error_type ppr_write_gallery (ppr_context_type context,
                                  const char *file_name,
                                  ppr_gallery_type gallery);

ppr_error_type ppr_read_gallery (ppr_context_type context,
                                 const char *file_name,
                                 ppr_gallery_type *gallery);

ppr_error_type ppr_write_similarity_matrix (ppr_context_type context,
                                            const char *file_name,
                                            ppr_similarity_matrix_type similarity_matrix);

ppr_error_type ppr_read_similarity_matrix (ppr_context_type context,
                                           const char *file_name,
                                           ppr_gallery_type *query_gallery,
                                           ppr_gallery_type *target_gallery,
                                           ppr_similarity_matrix_type *similarity_matrix);

ppr_error_type ppr_write_gallery_group (ppr_context_type context,
                                        const char *file_name,
                                        ppr_gallery_group_type gallery_group);

ppr_error_type ppr_read_gallery_group (ppr_context_type context,
                                       const char *file_name,
                                       ppr_gallery_group_type *gallery_group);

/*******************************************************************************
 * Free Functions
 ******************************************************************************/

void ppr_free_image (ppr_image_type image);

void ppr_free_face (ppr_face_type face);

void ppr_free_face_list (ppr_face_list_type face_list);

void ppr_free_image_results_list (ppr_image_results_list_type image_results_list);

void ppr_free_track_list (ppr_track_list_type track_list);

void ppr_free_dispersed_results(ppr_dispersed_results_type dispersed_results);
    
void ppr_free_landmark_list (ppr_landmark_list_type landmark_list);

void ppr_free_polygon (ppr_polygon_type polygon);

void ppr_free_string (ppr_string_type str);

void ppr_free_id_list (ppr_id_list_type id_list);

void ppr_free_score_list (ppr_score_list_type score_list);

void ppr_free_flat_data (ppr_flat_data_type flat_data);

void ppr_free_gallery (ppr_gallery_type gallery);

void ppr_free_similarity_matrix (ppr_similarity_matrix_type similarity_matrix);

void ppr_free_gallery_group (ppr_gallery_group_type gallery_group);

void ppr_free_cluster_list (ppr_cluster_list_type cluster_list);

/*******************************************************************************
 * Release Functions
 ******************************************************************************/

void ppr_release_face_reference (ppr_face_ref_type face_ref);

void ppr_release_gallery_reference (ppr_gallery_ref_type gallery_ref);

void ppr_release_similarity_matrix_reference (ppr_similarity_matrix_ref_type similarity_matrix_ref);
  
/*******************************************************************************
 * Finalize Functions
 ******************************************************************************/

ppr_error_type ppr_finalize_context (ppr_context_type context);

void ppr_finalize_sdk (void);

/******************************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* __PITTPATT_COMMON_H */
