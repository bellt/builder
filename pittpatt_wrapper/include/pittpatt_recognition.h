/**********************************************************************
 
  PittPatt Face Recognition Software Development Kit (PittPatt SDK)
  (C) Copyright 2004-2011 Pittsburgh Pattern Recognition

  This software is covered in part by the following patents:

     US Patent 6,829,384
     US Patent 7,194,114
     US Patent 7,848,566
     US Patent 7,881,505
     Pending US Patent Applications

  Portions of this product are manufactured under license from Carnegie
  Mellon University.

**********************************************************************/


#ifndef __PITTPATT_RECOGNITION_H
#define __PITTPATT_RECOGNITION_H

#include "pittpatt_types.h"
#include "pittpatt_errors.h"
#include "pittpatt_raw_image.h"

/* If C++ then we need to extern "C". Compiler defines __cplusplus */
#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 * Template Extraction Functions
 ******************************************************************************/

ppr_error_type ppr_is_template_extractable (ppr_context_type context,
                                            ppr_face_type face,
                                            int *is_extractable);

ppr_error_type ppr_extract_face_template (ppr_context_type context,
                                          ppr_image_type image,
                                          ppr_face_type *face);

ppr_error_type ppr_remove_face_template (ppr_context_type context,
                                         ppr_face_type *face);

ppr_error_type ppr_face_has_template (ppr_context_type context,
                                      ppr_face_type face,
                                      int *has_template);

/*******************************************************************************
 * Gallery Building and Editing Functions
 ******************************************************************************/

ppr_error_type ppr_create_gallery (ppr_context_type context,
                                   ppr_gallery_type *gallery);

ppr_error_type ppr_add_face (ppr_context_type context,
                             ppr_gallery_type *gallery,
                             ppr_face_type face,
                             int subject_id,
                             int face_id);

ppr_error_type ppr_remove_face (ppr_context_type context,
                                ppr_gallery_type *gallery,
                                int face_id);

ppr_error_type ppr_remove_subject (ppr_context_type context,
                                   ppr_gallery_type *gallery,
                                   int subject_id);

ppr_error_type ppr_merge_subjects (ppr_context_type context,
                                   ppr_gallery_type *gallery,
                                   int subject_id_1,
                                   int subject_id_2);

ppr_error_type ppr_change_face_subject_id (ppr_context_type context,
                                           ppr_gallery_type *gallery,
                                           int face_id,
                                           int new_subject_id);

ppr_error_type ppr_change_subject_id (ppr_context_type context,
                                      ppr_gallery_type *gallery,
                                      int old_subject_id,
                                      int new_subject_id);

ppr_error_type ppr_set_subject_diff (ppr_context_type context,
                                     ppr_gallery_type *gallery,
                                     int subject_id_1,
                                     int subject_id_2);

ppr_error_type ppr_clear_subject_diff (ppr_context_type context,
                                       ppr_gallery_type *gallery,
                                       int subject_id_1,
                                       int subject_id_2);

ppr_error_type ppr_clear_all_subject_diffs (ppr_context_type context,
                                            ppr_gallery_type *gallery);

ppr_error_type ppr_set_subject_string (ppr_context_type context,
                                       ppr_gallery_type *gallery,
                                       int subject_id,
                                       const char *str);

ppr_error_type ppr_set_gallery_string (ppr_context_type context,
                                       ppr_gallery_type *gallery,
                                       const char *str);

/*******************************************************************************
 * Query Functions
 ******************************************************************************/

ppr_error_type ppr_get_num_faces (ppr_context_type context,
                                  ppr_gallery_type gallery,
                                  int *num_faces);

ppr_error_type ppr_get_face_id_list (ppr_context_type context,
                                     ppr_gallery_type gallery,
                                     ppr_id_list_type *id_list);

ppr_error_type ppr_get_face_subject_id (ppr_context_type context,
                                        ppr_gallery_type gallery,
                                        int face_id,
                                        int *subject_id);

ppr_error_type ppr_get_subject_id_list (ppr_context_type context,
                                        ppr_gallery_type gallery,
                                        ppr_id_list_type *id_list);

ppr_error_type ppr_get_face_id_list_for_subject (ppr_context_type context,
                                                 ppr_gallery_type gallery,
                                                 int subject_id,
                                                 ppr_id_list_type *id_list);

ppr_error_type ppr_get_subject_string (ppr_context_type context,
                                       ppr_gallery_type gallery,
                                       int subject_id,
                                       ppr_string_type *str);

ppr_error_type ppr_get_gallery_string (ppr_context_type context,
                                       ppr_gallery_type gallery,
                                       ppr_string_type *str);

ppr_error_type ppr_get_subject_diffs (ppr_context_type context,
                                      ppr_gallery_type gallery,
                                      int subject_id,
                                      ppr_id_list_type *id_list);

ppr_error_type ppr_get_face_reference (ppr_context_type context,
                                       ppr_gallery_type gallery,
                                       int face_id,
                                       ppr_face_ref_type *face_ref);

/*******************************************************************************
 * Similarity Matrix Functions
 ******************************************************************************/

ppr_error_type ppr_compare_galleries (ppr_context_type context,
                                      ppr_gallery_type query_gallery,
                                      ppr_gallery_type target_gallery,
                                      ppr_similarity_matrix_type *similarity_matrix);

ppr_error_type ppr_update_similarity_scores (ppr_context_type context,
                                             ppr_similarity_matrix_type *similarity_matrix);

ppr_error_type ppr_get_self_similarity_matrix_reference (ppr_context_type context,
                                                         ppr_gallery_type gallery,
                                                         ppr_similarity_matrix_ref_type *self_similarity_matrix_ref);

ppr_error_type ppr_get_face_similarity_score (ppr_context_type context,
                                              ppr_similarity_matrix_type similarity_matrix,
                                              int query_face_id,
                                              int target_face_id,
                                              float *score);

ppr_error_type ppr_get_subject_similarity_score (ppr_context_type context,
                                                 ppr_similarity_matrix_type similarity_matrix,
                                                 int query_subject_id,
                                                 int target_subject_id,
                                                 float *score);
                                                             
ppr_error_type ppr_get_ranked_subject_list (ppr_context_type context,
                                            ppr_similarity_matrix_type similarity_matrix,
                                            int query_subject_id,
                                            int max_list_length,
                                            float threshold,
                                            ppr_score_list_type *score_list);

ppr_error_type ppr_get_best_matching_subjects (ppr_context_type context,
                                               ppr_similarity_matrix_type similarity_matrix,
                                               ppr_id_list_type query_subject_ids,
                                               int queries_are_unique,
                                               ppr_score_list_type *score_list);

ppr_error_type ppr_get_best_matching_subjects_for_clusters (ppr_context_type context,
                                                            ppr_similarity_matrix_type similarity_matrix,
                                                            ppr_cluster_list_type query_clusters,
                                                            int queries_are_unique,
                                                            ppr_score_list_type *score_list);

ppr_error_type ppr_enable_transitive_matching (ppr_context_type context,
                                               ppr_similarity_matrix_type *similarity_matrix);

ppr_error_type ppr_disable_transitive_matching (ppr_context_type context,
                                                ppr_similarity_matrix_type *similarity_matrix);

ppr_error_type ppr_is_transitive_matching_enabled (ppr_context_type context,
                                                   ppr_similarity_matrix_type similarity_matrix,
                                                   int *is_enabled);

ppr_error_type ppr_get_gallery_references (ppr_context_type context,
                                           ppr_similarity_matrix_type similarity_matrix,
                                           ppr_gallery_ref_type *query_gallery_ref,
                                           ppr_gallery_ref_type *target_gallery_ref);

/*******************************************************************************
 * Clustering Functions
 ******************************************************************************/

ppr_error_type ppr_cluster_gallery (ppr_context_type context,
                                    ppr_gallery_type *gallery,
                                    int clustering_aggressiveness,
                                    ppr_cluster_list_type *cluster_list);

/*******************************************************************************
 * Representative Faces Functions
 ******************************************************************************/

ppr_error_type ppr_get_representative_faces_for_subjects (ppr_context_type context,
                                                          ppr_gallery_type gallery,
                                                          int num_representative_faces,
                                                          ppr_id_list_type *representative_face_id_list);

ppr_error_type ppr_trim_subjects_to_representative_faces (ppr_context_type context,
                                                          ppr_gallery_type *gallery,
                                                          int num_representative_faces);

/******************************************************************************/


#ifdef __cplusplus
}
#endif

#endif /* __PITTPATT_RECOGNITION_H */
