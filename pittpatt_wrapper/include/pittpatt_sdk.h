/**********************************************************************
 
  PittPatt Face Recognition Software Development Kit (PittPatt SDK)
  (C) Copyright 2004-2011 Pittsburgh Pattern Recognition

  This software is covered in part by the following patents:

     US Patent 6,829,384
     US Patent 7,194,114
     US Patent 7,848,566
     US Patent 7,881,505
     Pending US Patent Applications

  Portions of this product are manufactured under license from Carnegie
  Mellon University.

**********************************************************************/


#ifndef __PITTPATT_SDK_H
#define __PITTPATT_SDK_H

#include "pittpatt_common.h"
#include "pittpatt_detection.h"
#include "pittpatt_errors.h"
#include "pittpatt_gallery_group.h"
#include "pittpatt_raw_image.h"
#include "pittpatt_recognition.h"
#include "pittpatt_types.h"

#endif /* __PITTPATT_SDK_H */
