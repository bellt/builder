/**********************************************************************
 
  PittPatt Face Recognition Software Development Kit (PittPatt SDK)
  (C) Copyright 2004-2011 Pittsburgh Pattern Recognition

  This software is covered in part by the following patents:

     US Patent 6,829,384
     US Patent 7,194,114
     US Patent 7,848,566
     US Patent 7,881,505
     Pending US Patent Applications

  Portions of this product are manufactured under license from Carnegie
  Mellon University.

**********************************************************************/


#ifndef __PITTPATT_ERRORS_H
#define __PITTPATT_ERRORS_H

/* If C++ then we need to extern "C". Compiler defines __cplusplus */
#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 * Enumerated Types
 ******************************************************************************/

typedef enum
{
  PPR_SUCCESS,
  PPR_NULL_CONTEXT,
  PPR_CORRUPT_CONTEXT,
  PPR_CONTEXT_NOT_INITIALIZED,
  PPR_SDK_ALREADY_INITIALIZED,
  PPR_SDK_NOT_INITIALIZED,
  PPR_NULL_IMAGE,
  PPR_NULL_FACE,
  PPR_NULL_GALLERY,
  PPR_NULL_SIMILARITY_MATRIX,
  PPR_NULL_GALLERY_GROUP,
  PPR_NULL_DESTINATION,
  PPR_NULL_REFERENCE_POINTER,
  PPR_NULL_FILE_NAME,
  PPR_NULL_MODELS_PATH,
  PPR_INVALID_MODELS_PATH,
  PPR_NULL_STRING,
  PPR_NULL_LICENSE_ID,
  PPR_NULL_LICENSE_KEY,
  PPR_UNAUTHORIZED_LICENSE,
  PPR_LICENSE_ID_NOT_SET,
  PPR_LICENSE_KEY_NOT_SET,
  PPR_INVALID_LICENSE,
  PPR_INVALID_LICENSE_VERSION,
  PPR_LICENSE_EXPIRED,
  PPR_REQUIRES_RECOGNITION_ENABLED_LICENSE,
  PPR_LANDMARKS_REQUIRE_DETECTION,
  PPR_TRACKING_REQUIRES_DETECTION,
  PPR_DETECT_BEST_FACE_REQUIRES_LANDMARKS,
  PPR_CALLBACK_FUNCTION_REQUIRES_BATCH_MODE_DETECTION,
  PPR_DISCARD_COMPLETED_TRACKS_MUST_BE_DISABLED,
  PPR_DISCARD_COMPLETED_TRACKS_REQUIRES_NO_VISUAL_INSTANCES,
  PPR_EXTRACT_THUMBNAILS_REQUIRES_TRACKING_DISABLED,
  PPR_AUTOMATIC_EXTRACTION_REQUIRES_TRACKING_DISABLED,
  PPR_AUTOMATIC_EXTRACTION_REQUIRES_DETECTION,
  PPR_AUTOMATIC_EXTRACTION_REQUIRES_LANDMARKS,
  PPR_MANUAL_LANDMARKS_REQUIRES_SERIAL_MODE_DETECTION,
  PPR_MULTI_POSE_RECOGNIZER_REQUIRES_COMPREHENSIVE_LANDMARK_RANGE,
  PPR_INVALID_MIN_SIZE,
  PPR_INVALID_MAX_SIZE,
  PPR_INVALID_ADAPTIVE_MIN_SIZE,
  PPR_INVALID_ADAPTIVE_MAX_SIZE,
  PPR_INVALID_YAW,
  PPR_INVALID_PITCH,
  PPR_INVALID_ROLL,
  PPR_INVALID_POLYGON,
  PPR_INVALID_STRING,
  PPR_INVALID_ID_LIST,
  PPR_INVALID_SCORE_LIST,
  PPR_INVALID_FACE_ATTRIBUTES,
  PPR_INVALID_LANDMARK_LIST,
  PPR_INVALID_CLUSTER_LIST,
  PPR_INVALID_LANDMARK_RANGE,
  PPR_INVALID_SEARCH_PRUNING_AGGRESSIVENESS,
  PPR_INVALID_NUM_DETECTION_THREADS,
  PPR_INVALID_DETECTION_THRESHOLD,
  PPR_INVALID_IMAGE_QUEUE_SIZE,
  PPR_INVALID_SEARCH_REGION,
  PPR_INVALID_NUM_VISUAL_INSTANCES,
  PPR_INVALID_TRACKING_CUTOFF,
  PPR_INVALID_RECOGNIZER,
  PPR_INVALID_CLUSTERING_AGGRESSIVENESS,
  PPR_INVALID_NUM_REPRESENTATIVE_FACES,
  PPR_INVALID_STRAGGLER_CUTOFF,
  PPR_INVALID_BOUNDING_BOX_STYLE,
  PPR_INVALID_WIDTH,
  PPR_INVALID_HEIGHT,
  PPR_INVALID_WIDTH_HEIGHT_COMBINATION,
  PPR_INVALID_PADDING,
  PPR_INVALID_FILE_NAME,
  PPR_INVALID_NUM_PIXELS,
  PPR_INVALID_MAX_NUM_FACES,
  PPR_INVALID_COLOR_SPACE,
  PPR_INVALID_RAW_IMAGE,
  PPR_INVALID_DIMENSIONS,
  PPR_INVALID_FACE_LIST,
  PPR_INVALID_IMAGE_RESULTS_LIST,
  PPR_INVALID_TRACK_SUMMARY,
  PPR_INVALID_VISUAL_INSTANCE,
  PPR_INVALID_TRACK,
  PPR_INVALID_TRACK_LIST,
  PPR_INVALID_FLAT_DATA,
  PPR_INVALID_LANDMARK_CATEGORY,
  PPR_INVALID_LENGTH,
  PPR_INVALID_MAX_LIST_LENGTH,
  PPR_NO_THUMBNAIL,
  PPR_CORRUPT_FACE,
  PPR_CORRUPT_GALLERY,
  PPR_CORRUPT_SIMILARITY_MATRIX,
  PPR_CORRUPT_GALLERY_GROUP,
  PPR_UNABLE_TO_WRITE_FILE,
  PPR_UNABLE_TO_READ_FILE,
  PPR_FILE_NOT_FOUND,
  PPR_IO_ERROR,
  PPR_REQUIRES_DETECTION_ENABLED,
  PPR_REQUIRES_LANDMARKS_ENABLED,
  PPR_REQUIRES_TRACKING_ENABLED,
  PPR_REQUIRES_EXTRACTION_ENABLED,
  PPR_REQUIRES_COMPARISON_ENABLED,
  PPR_REQUIRES_EXTRACTION_OR_COMPARISON_ENABLED,
  PPR_REQUIRES_TRACKING_DISABLED,
  PPR_REQUIRES_SERIAL_MODE_DETECTION,
  PPR_REQUIRES_BATCH_MODE_DETECTION,
  PPR_ID_NOT_FOUND,
  PPR_FACE_ID_NOT_FOUND,
  PPR_SUBJECT_ID_NOT_FOUND,
  PPR_GALLERY_ID_NOT_FOUND,
  PPR_REQUIRES_UNIQUE_ID,
  PPR_SIMILARITY_MATRIX_NOT_CURRENT,
  PPR_REQUIRES_SELF_SIMILARITY_MATRIX,
  PPR_REQUIRES_QUERY_TARGET_SIMILARITY_MATRIX,
  PPR_REQUIRES_DIFFERENT_GALLERIES,
  PPR_REQUIRES_DIFFERENT_DESTINATIONS,
  PPR_CANNOT_FLATTEN_SELF_SIMILARITY_MATRIX,
  PPR_REQUIRES_SAME_RECOGNIZER,
  PPR_GALLERY_ALREADY_IN_GALLERY_GROUP,
  PPR_INCONSISTENT_IMAGE_DIMENSIONS,
  PPR_INVALID_VALUE_TYPE,
  PPR_CANNOT_MODIFY_TEMPLATES,
  PPR_FACE_ALREADY_HAS_LANDMARKS,
  PPR_CALLBACK_ERROR,
  PPR_OUT_OF_MEMORY,
  PPR_BAD_CAST,
  PPR_BAD_EXCEPTION,
  PPR_BAD_TYPEID,
  PPR_IO_STREAM_FAILURE,
  PPR_LOGIC_ERROR,
  PPR_RUNTIME_ERROR,
  PPR_COMMAND_LINE_ERROR,
  PPR_FATAL_ERROR,
  PPR_UNIMPLEMENTED,
  PPR_UNSUPPORTED_USE,
  PPR_UNHANDLED_ERROR,
  PPR_UNHANDLED_STD_ERROR,
  PPR_UNKNOWN_ERROR,
  PPR_NUM_ERROR_MESSAGES
} ppr_error_type;

/******************************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* __PITTPATT_ERRORS_H */
