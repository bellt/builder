/**********************************************************************
 
  PittPatt Face Recognition Software Development Kit (PittPatt SDK)
  (C) Copyright 2004-2011 Pittsburgh Pattern Recognition

  This software is covered in part by the following patents:

     US Patent 6,829,384
     US Patent 7,194,114
     US Patent 7,848,566
     US Patent 7,881,505
     Pending US Patent Applications

  Portions of this product are manufactured under license from Carnegie
  Mellon University.

**********************************************************************/


#ifndef __PITTPATT_TYPES_H
#define __PITTPATT_TYPES_H

#include <stdio.h>

#include "pittpatt_errors.h"
#include "pittpatt_raw_image.h"

/* If C++ then we need to extern "C". Compiler defines __cplusplus */
#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 * Constants
 ******************************************************************************/

#define PPR_MIN_MIN_SIZE                       -2
#define PPR_MAX_MAX_SIZE                        100

#define PPR_MAX_SEARCH_PRUNING_AGGRESSIVENESS   4

#define PPR_MIN_TRACKING_CUTOFF                -5    /* Most permissive  */
#define PPR_MAX_TRACKING_CUTOFF                 5    /* Most restrictive */

#define PPR_MAX_CLUSTERING_AGGRESSIVENESS       10

#define PPR_SIMILARITY_SCORE_SAME               100.0f
#define PPR_SIMILARITY_SCORE_DIFF              -100.0f
#define PPR_SIMILARITY_SCORE_NOT_COMPARABLE    -500.0f

/*******************************************************************************
 * Basic Types
 ******************************************************************************/

typedef struct ppr_context_struct* ppr_context_type;
typedef struct ppr_image_struct* ppr_image_type;
typedef struct ppr_face_struct* ppr_face_type;
typedef struct ppr_gallery_struct* ppr_gallery_type;
typedef struct ppr_similarity_matrix_struct* ppr_similarity_matrix_type;
typedef struct ppr_gallery_group_struct* ppr_gallery_group_type;

/*******************************************************************************
 * Reference Types
 ******************************************************************************/

typedef ppr_face_type* ppr_face_ref_type;
typedef ppr_gallery_type* ppr_gallery_ref_type;
typedef ppr_similarity_matrix_type* ppr_similarity_matrix_ref_type;

/*******************************************************************************
 * Enumerated Types
 ******************************************************************************/

typedef enum
{
  PPR_LANDMARK_RANGE_FRONTAL,
  PPR_LANDMARK_RANGE_EXTENDED,
  PPR_LANDMARK_RANGE_FULL,
  PPR_LANDMARK_RANGE_COMPREHENSIVE,
  PPR_NUM_LANDMARK_RANGES
} ppr_landmark_range_type;

typedef enum
{
  PPR_RECOGNIZER_FRONTAL,
  PPR_RECOGNIZER_MULTI_POSE,
  PPR_RECOGNIZER_FAST_MULTI_POSE,
  PPR_NUM_RECOGNIZERS
} ppr_recognizer_type;

typedef enum
{
  PPR_LANDMARK_CATEGORY_LEFT_EYE,
  PPR_LANDMARK_CATEGORY_RIGHT_EYE,
  PPR_LANDMARK_CATEGORY_NOSE_BASE,
  PPR_LANDMARK_CATEGORY_NOSE_BRIDGE,
  PPR_LANDMARK_CATEGORY_EYE_NOSE,
  PPR_LANDMARK_CATEGORY_LEFT_UPPER_CHEEK,
  PPR_LANDMARK_CATEGORY_LEFT_LOWER_CHEEK,
  PPR_LANDMARK_CATEGORY_RIGHT_UPPER_CHEEK,
  PPR_LANDMARK_CATEGORY_RIGHT_LOWER_CHEEK,
  PPR_NUM_LANDMARK_CATEGORIES
} ppr_landmark_category_type;

typedef enum
{
  PPR_BOUNDING_BOX_STYLE_DEFAULT,
  PPR_BOUNDING_BOX_STYLE_TIGHT,
  PPR_NUM_BOUNDING_BOX_STYLES
} ppr_bounding_box_style_type;

/*******************************************************************************
 * Primitive Structure Types
 ******************************************************************************/

typedef struct
{
  float x;
  float y;
} ppr_coordinate_type;

typedef struct
{
  float width;
  float height;
} ppr_dimensions_type;

typedef struct
{
  float yaw;
  float pitch;
  float roll;
} ppr_rotation_type;

typedef struct
{
  int length;
  char *str;
} ppr_string_type;

typedef struct
{
  int num_points;
  ppr_coordinate_type *points;
} ppr_polygon_type;

typedef struct
{
  size_t length;
  void *data;
} ppr_flat_data_type;

/*******************************************************************************
 * Result Structure Types
 ******************************************************************************/

typedef struct
{
  ppr_landmark_category_type category;
  ppr_coordinate_type position;
} ppr_landmark_type;

typedef struct
{
  int length;
  ppr_landmark_type *landmarks;
} ppr_landmark_list_type;

typedef struct
{
  int frame_number;
  int track_id;
  int confidence_level;
} ppr_tracking_info_type;

typedef struct
{
  ppr_coordinate_type position;
  ppr_dimensions_type dimensions;
  ppr_rotation_type rotation;
  float size;
  float confidence;
  int num_landmarks;
  int has_thumbnail;
  ppr_tracking_info_type tracking_info;
} ppr_face_attributes_type;

typedef struct
{
  int length;
  ppr_face_type *faces;
  ppr_string_type source_name;
} ppr_face_list_type;

typedef struct
{
  int length;
  ppr_face_list_type *face_lists;
} ppr_image_results_list_type;

typedef struct
{
  int track_id;
  int start_frame_number;
  int stop_frame_number;
  ppr_coordinate_type start_position;
  ppr_coordinate_type stop_position;
  ppr_dimensions_type min_dimensions;
  ppr_dimensions_type max_dimensions;
  ppr_dimensions_type avg_dimensions;
  float min_confidence;
  float max_confidence;
  float avg_confidence;
} ppr_track_summary_type;

typedef struct
{
  int frame_number;
  ppr_raw_image_type image;
} ppr_visual_instance_type;

typedef struct
{
  int length;
  ppr_visual_instance_type *instances;
} ppr_visual_instance_list_type;

typedef struct
{
  ppr_track_summary_type track_summary;
  ppr_face_list_type tracked_faces;
  ppr_visual_instance_list_type visual_instances;
} ppr_track_type;

typedef struct
{
  int length;
  ppr_track_type *tracks;
  ppr_string_type source_name;
  int start_frame;
  ppr_image_results_list_type frame_results;
} ppr_track_list_type;

typedef struct
{
  int length;
  int *frame_numbers;
  int *frame_face_indices;
  int *track_indices;
} ppr_dispersed_results_type;

typedef struct
{
  int length;
  int *ids;
} ppr_id_list_type;

typedef struct
{
  int length;
  float *scores;
  int *ids;
} ppr_score_list_type;

typedef struct
{
  int length;
  ppr_id_list_type *clusters;
} ppr_cluster_list_type;

/*******************************************************************************
 * Initialization Structure Types
 ******************************************************************************/

typedef struct
{
  int enable;
  ppr_coordinate_type top_left;
  ppr_coordinate_type bottom_right;
} ppr_search_region_type;

typedef struct
{
  int enable;
  int min_size;
  int max_size;
  float adaptive_min_size;
  float adaptive_max_size;
  float threshold;
  ppr_search_region_type search_region;
  int use_serial_face_detection;
  int num_threads;
  int image_queue_size;
  int search_pruning_aggressiveness;
  int detect_best_face_only;
  int extract_thumbnails;
  int (*results_callback)(int index, ppr_image_type image, ppr_face_list_type face_list);
} ppr_detection_settings_type;

typedef struct
{
  int enable;
  ppr_landmark_range_type landmark_range;
  int manually_detect_landmarks;
} ppr_landmark_settings_type;

typedef struct
{
  int enable;
  int cutoff;
  int num_visual_instances;
  int discard_completed_tracks;
  int enable_shot_boundary_detection;
  float shot_boundary_threshold;
} ppr_tracking_settings_type;

typedef struct
{
  int enable_extraction;
  int enable_comparison;
  ppr_recognizer_type recognizer;
  int num_comparison_threads;
  int automatically_extract_templates;
  int extract_thumbnails;
} ppr_recognition_settings_type;

typedef struct
{
  ppr_detection_settings_type detection;
  ppr_landmark_settings_type landmarks;
  ppr_tracking_settings_type tracking;
  ppr_recognition_settings_type recognition;
} ppr_settings_type;

/******************************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* __PITTPATT_TYPES_H */
