/**********************************************************************
 
  PittPatt Face Recognition Software Development Kit (PittPatt SDK)
  (C) Copyright 2004-2011 Pittsburgh Pattern Recognition

  This software is covered in part by the following patents:

     US Patent 6,829,384
     US Patent 7,194,114
     US Patent 7,848,566
     US Patent 7,881,505
     Pending US Patent Applications

  Portions of this product are manufactured under license from Carnegie
  Mellon University.

**********************************************************************/


#ifndef __PITTPATT_DETECTION_H
#define __PITTPATT_DETECTION_H

#include "pittpatt_types.h"
#include "pittpatt_errors.h"
#include "pittpatt_raw_image.h"

/* If C++ then we need to extern "C". Compiler defines __cplusplus */
#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 * Batch Mode Detection Functions
 ******************************************************************************/

ppr_error_type ppr_add_image_to_detection_queue (ppr_context_type context,
                                                 ppr_image_type image);

ppr_error_type ppr_get_detection_results (ppr_context_type context,
                                          ppr_image_results_list_type *image_results_list);

/*******************************************************************************
 * Serial Mode Detection Functions
 ******************************************************************************/

ppr_error_type ppr_detect_faces (ppr_context_type context,
                                 ppr_image_type image,
                                 ppr_face_list_type *face_list);

ppr_error_type ppr_detect_face_landmarks (ppr_context_type context,
                                          ppr_image_type image,
                                          ppr_face_type *face);

/*******************************************************************************
 * Track Query Functions
 ******************************************************************************/

ppr_error_type ppr_get_completed_tracks (ppr_context_type context,
                                         ppr_track_list_type *track_list);

ppr_error_type ppr_finalize_tracks (ppr_context_type context);

/*******************************************************************************
 * Track Manipulation Functions
 ******************************************************************************/

ppr_error_type ppr_get_dispersed_faces_from_track (ppr_context_type context,
                                                   ppr_track_type track,
                                                   int max_num_faces,
                                                   ppr_face_list_type *face_list);

ppr_error_type ppr_get_dispersed_faces_from_track_list (ppr_context_type context,
                                                        ppr_track_list_type track_list,
                                                        int max_num_faces_per_track,
                                                        ppr_dispersed_results_type *dispersed_results);

/******************************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* __PITTPATT_DETECTION_H */
