/**********************************************************************
 
  PittPatt Face Recognition Software Development Kit (PittPatt SDK)
  (C) Copyright 2004-2011 Pittsburgh Pattern Recognition

  This software is covered in part by the following patents:

     US Patent 6,829,384
     US Patent 7,194,114
     US Patent 7,848,566
     US Patent 7,881,505
     Pending US Patent Applications

  Portions of this product are manufactured under license from Carnegie
  Mellon University.

**********************************************************************/


#ifndef __PITTPATT_GALLERY_GROUP_H
#define __PITTPATT_GALLERY_GROUP_H

#include "pittpatt_types.h"
#include "pittpatt_errors.h"
#include "pittpatt_recognition.h"

/* If C++ then we need to extern "C". Compiler defines __cplusplus */
#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
 * Gallery Group Building and Editing Functions
 ******************************************************************************/

ppr_error_type ppr_create_gallery_group (ppr_context_type context,
                                         ppr_gallery_group_type *gallery_group);

ppr_error_type ppr_move_gallery_to_gallery_group (ppr_context_type context,
                                                  ppr_gallery_group_type *gallery_group,
                                                  ppr_gallery_type *gallery,
                                                  int gallery_id);

ppr_error_type ppr_remove_gallery_from_gallery_group (ppr_context_type context,
                                                      ppr_gallery_group_type *gallery_group,
                                                      int gallery_id);

ppr_error_type ppr_set_subject_diff_in_gallery_group (ppr_context_type context,
                                                      ppr_gallery_group_type *gallery_group,
                                                      int subject_id_1,
                                                      int subject_id_2);

ppr_error_type ppr_clear_subject_diff_in_gallery_group (ppr_context_type context,
                                                        ppr_gallery_group_type *gallery_group,
                                                        int subject_id_1,
                                                        int subject_id_2);

ppr_error_type ppr_clear_all_subject_diffs_in_gallery_group (ppr_context_type context,
                                                             ppr_gallery_group_type *gallery_group);

ppr_error_type ppr_set_num_representative_faces (ppr_context_type context,
                                                 ppr_gallery_group_type *gallery_group,
                                                 int num_representative_faces);

ppr_error_type ppr_set_straggler_cutoff (ppr_context_type context,
                                         ppr_gallery_group_type *gallery_group,
                                         int straggler_cutoff);

/*******************************************************************************
 * Query Functions
 ******************************************************************************/

ppr_error_type ppr_get_num_galleries_in_gallery_group (ppr_context_type context,
                                                       ppr_gallery_group_type gallery_group,
                                                       int *num_galleries);

ppr_error_type ppr_get_gallery_id_list_from_gallery_group (ppr_context_type context,
                                                           ppr_gallery_group_type gallery_group,
                                                           ppr_id_list_type *id_list);

ppr_error_type ppr_get_subject_diffs_from_gallery_group (ppr_context_type context,
                                                         ppr_gallery_group_type gallery_group,
                                                         int subject_id,
                                                         ppr_id_list_type *id_list);

ppr_error_type ppr_get_subject_diff_pairs_from_gallery_group (ppr_context_type context,
                                                              ppr_gallery_group_type gallery_group,
                                                              ppr_id_list_type *id_list_a,
                                                              ppr_id_list_type *id_list_b);

ppr_error_type ppr_get_num_representative_faces (ppr_context_type context,
                                                 ppr_gallery_group_type gallery_group,
                                                 int *num_representative_faces);

ppr_error_type ppr_get_straggler_cutoff (ppr_context_type context,
                                         ppr_gallery_group_type gallery_group,
                                         int *straggler_cutoff);

ppr_error_type ppr_get_gallery_reference_from_gallery_group (ppr_context_type context,
                                                             ppr_gallery_group_type gallery_group,
                                                             int gallery_id,
                                                             ppr_gallery_ref_type *gallery_ref);

/*******************************************************************************
 * Comparison Functions
 ******************************************************************************/

ppr_error_type ppr_cluster_gallery_group (ppr_context_type context,
                                          ppr_gallery_group_type *gallery_group,
                                          int clustering_aggressiveness,
                                          ppr_cluster_list_type *cluster_list);

ppr_error_type ppr_get_ranked_subject_list_from_gallery_group (ppr_context_type context,
                                                               ppr_gallery_group_type gallery_group,
                                                               int query_subject_id,
                                                               int max_list_length,
                                                               float threshold,
                                                               ppr_score_list_type *score_list);

/******************************************************************************/
  
#ifdef __cplusplus
}
#endif
  
#endif /* __PITTPATT_GALLERY_GROUP_H */
