﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfaceBuilder
{  
   public interface IMatchResult
   {
      ISubject subject { get; set; }
      double score { get; set; }
   }  
}
