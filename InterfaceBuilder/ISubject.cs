﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfaceBuilder
{
   public interface ISubject
   {
      int id { get; set; }
   }
}
