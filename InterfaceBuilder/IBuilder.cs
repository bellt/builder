﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfaceBuilder
{ 
   public interface IBuilder
   {
      status initialize_builder();
      status destroy_builder();
   }

   public enum status
   {
      SUCCESS,
      FAILURE
   } 
}
